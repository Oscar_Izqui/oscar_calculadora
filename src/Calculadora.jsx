import React, { useState, useEffect } from "react"
import { Row, Col } from "reactstrap"
import styled from "styled-components"

import Pantalla from "./Pantalla.jsx"
import Tecla from "./Tecla.jsx"

const Teclado = styled.div`
    width: 90%;
    margin: 16px 5%;
`;

const CalculadoraStyled = styled.div`
    width: 100%;
    height: 340px;
    background-color: darkgrey;
    padding-top: 15px;
`;

const CalculadoraObj = () => {

    const [input, setInput] = useState(0);
    const [calculo, setCalculo] = useState("");
    const [refreshInput, setRefreshInput] = useState(true);
    const [refreshCalculo, setRefreshCalculo] = useState(false);

    const clicar = (value) => {

        if (value >= 0 && value <= 9) {
            if (input * 1 === 0 || refreshInput) {
                setInput(value);
                setRefreshInput(false);
            }
            else setInput(input + value);
        }
        else if (value === "*" ||
            value === "/" ||
            value === "-" ||
            value === "+") {
            setCalculo(() => {
                if (calculo === "" || refreshCalculo) {
                    setRefreshCalculo(false);
                    return input + value;
                }
                else return calculo + input + value;
            });
            setRefreshInput(true);
        }
        else if (value === "=") {
            setCalculo(() => {
                if (calculo === "") return input + value;
                else return calculo + input + value;
            });
            setRefreshInput(true);
            setRefreshCalculo(true);
        }
        else if (value === "C") {
            setInput(0);
            setRefreshInput(true);
        }
    }

    useEffect(() => {
        if (refreshCalculo) {
            console.log("calculando");
            /*
                FUNCION PARA CALCULAR LA OPERACION = () => { return setInput(resultadoOperacion); }
            */
        }
    }, [refreshCalculo])

    return (
        <Col sm={{ size: 6, offset: 3 }}>
            <CalculadoraStyled>
                <Row>
                    <Pantalla input={input} calculo={calculo} />
                </Row>
                <Teclado>
                    <Row>
                        <Tecla value="1" clicar={clicar} />
                        <Tecla value="2" clicar={clicar} />
                        <Tecla value="3" clicar={clicar} />
                        <Tecla value="*" clicar={clicar} />

                        <Tecla value="4" clicar={clicar} />
                        <Tecla value="5" clicar={clicar} />
                        <Tecla value="6" clicar={clicar} />
                        <Tecla value="/" clicar={clicar} />

                        <Tecla value="7" clicar={clicar} />
                        <Tecla value="8" clicar={clicar} />
                        <Tecla value="9" clicar={clicar} />
                        <Tecla value="-" clicar={clicar} />

                        <Tecla value="0" clicar={clicar} />
                        <Tecla value="C" clicar={clicar} />
                        <Tecla value="=" clicar={clicar} />
                        <Tecla value="+" clicar={clicar} />
                    </Row>
                </Teclado>
            </CalculadoraStyled>
        </Col>
    );
};

export default CalculadoraObj;