import './App.css';
import React from "react"
import { Container, Row } from "reactstrap"

import Calculadora from "./Calculadora.jsx"

function App() {
  return (
    <div className="App">
      <h1>Calculadora</h1>
      <Container>
        <Row>          
            <Calculadora/>
        </Row>
      </Container>
    </div>
  );
}

export default App;
