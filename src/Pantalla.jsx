import React from "react"
import { Col } from "reactstrap"
import styled from "styled-components"

const PantallaStyle = styled.div`
    width: 90%;
    height: 60px;
    background-color: white;
    line-height: 60px;
    text-align: right;
    padding: 0px 10px;
    margin: 0% 5%;
    position: relative;
    font-size: 23px;
`;

const Pantallica = styled.div`
    position: absolute;
    top: 5px;
    left: 5px;
    padding: 0% 10%;
    font-size: 13px;
`;

const PantallaObj = (props) => {
    return (
        <Col xs={{ size: 12 }}>
            <PantallaStyle>
                {props.input}
            </PantallaStyle>
            <Pantallica>
                {props.calculo}
            </Pantallica>
        </Col>
    );
};

export default PantallaObj;