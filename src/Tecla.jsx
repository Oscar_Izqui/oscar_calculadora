import React from "react"
import { Col } from "reactstrap"
import styled from "styled-components"

const TeclaStyle = styled.div`
    width: 100%;
    height: 50px;
    background-color: dimgrey;
    color: white;
    line-height: 50px;
    text-align: center;
    margin-bottom: 10px;
    user-select: none;
    cursor: pointer;

    &:active {
        background-color: black;
    }
`;

const TeclaObj = (props) => {

    return (
        <Col xs={3}>
            <TeclaStyle onClick={() => props.clicar(props.value)} >{props.value}</TeclaStyle>
        </Col>
    );
};

export default TeclaObj;